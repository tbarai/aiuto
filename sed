# To replace all occurrences of "day" with "night" and write to stdout:
sed 's/day/night/g' file.txt

# To replace all occurrences of "day" with "night" within file.txt:
sed -i 's/day/night/g' file.txt

# Delimiter: conventionally a slash but it can be anything else
sed 's_/usr/local/bin_/common/bin_' file.txt

# To replace all occurrences of "day" with "night" on stdin:
echo 'It is daytime' | sed 's/day/night/g'

# To remove leading spaces
sed -i -r 's/^\s+//g' file.txt

# Remove empty lines and print results to stdout:
sed '/^$/d' file.txt

# (for ffmpeg) to each line, prepend "file './" and appends "'"
sed -i 's/^/file '"'"'.\//' txt && sed -i 's/$/'"'"'/' txt 
