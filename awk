# Select first field from each line
awk '{print $1}' 

# print the last field of each line
awk '{ print $NF }'

# delete ALL blank lines from a file 
awk '/./'

# print only lines of 65 characters or longer
awk 'length > 64'

# print section of file based on line numbers (lines 8-12, inclusive)
awk 'NR==8,NR==12'

# remove duplicate, nonconsecutive lines
awk '! a[$0]++'                     # most concise script
awk '!($0 in a) {a[$0];print}'      # most efficient script

# align all text flush right on a 79-column width
awk '{printf "%79s\n", $0}' file*

# center all text on a 79-character width
awk '{l=length();s=int((79-l)/2); printf "%"(s+l)"s\n",$0}' file*

# print every line with more than 4 fields
awk 'NF > 4'

# print every line where the value of the last field is > 4
awk '$NF > 4'
