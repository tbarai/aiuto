:e              reload file
:x              write file and exit

W               next start of whitespace-delimited word
E               next end of whitespace-delimited word
B               previous start of whitespace-delimited word
0               start of line
$               end of line

a               append after the cursor
A               append at the end of the line
i               insert before the cursor
I               insert at the beginning of the line
o               create a new line under the cursor
O               create a new line above the cursor

y{motion}       yank text that {motion} moves over
d{motion}       delete text that {motion} moves over

50k         moves 50 lines up
2dw         deletes 2 words
5yy         copies 5 lines
42G         go to line 42

# Open a new tab window
:tabnew

# Used like :e but opens in a new tab
:tabe

# Go to the next tab on the right
CTRL-<PgUp>

# Go to the next tab on the left
CTRL-<PgDn>


### Splits

Open new split panes to right and bottom - more natural than Vim’s default:
* `set splitbelow`
* `set splitright`

# Split vertical: 
:vsp (filename)

# Split horizontal:
:sp (filename)

Swap top/bottom or left/right split
* `CTRL-w R`

Resize: Max out the height of the current split
* `CTRL-w _`

Resize: Max out the width of the current split
* `CTRL-w |`

# move the cursor up to a window
CTRL-w j

# move the cursor down a window
CTRL-w k

# Cycle through all the splits
CTRL-W CTRL-W

# Normalize all split sizes, very handy when resizing terminal
CTRL-w =


# Insert `{string}` at the start of block on every line of the block
blockwise selection, I, {string}, <ESC>
 
 
# per spostarsi verticalmente all'interno di righe molto molto lunghe 
g + UP / g + DOWN

# eseguire comando esterno: 
:!  seguito dal comando
 
# inserire il contenuto di un file:
:r NOMEFILE

# inserire l'output di un comando:
:r !COMANDO
 
# Per salvare una porzione di file:
v movimento :w NOMEFILE
 
# delete up to 'pattern' 
d, /, pattern

# delete paragraph
d,a,p


# cancella tutte le linee che non contengono 'pattern' 
:v/pattern/d
 
# sposta il testo a destra di un TAB 
>
 
# sostituisce ogni spazio con un a capo  
:s/ /\r/g/
 
# trova ogni occorrenza e propone di sostituire
:%s/vecchio/nuovo/gc
 
# search forwards for the exact word under the cursor 
*

# search backwards 
#
 
# format the highlighted lines [Visual mode] 
:gq

# annulla tutte le modifiche nella linea corrente
U

# Modalità Replace: ogni carattere battuto ricopre un carattere esistente
R

# Marks
:ma 		# imposta alla posizione corrente il mark A
'a oppure `a	# si sposta al mark A

# Registers
"+ 		# special register that refers to the system clipboard
"0p		# registers precedenti
"1p  "2p  etc	#

# the '.' command repeats the last complete, combined editing command (not movement commands)

# diff
vim -d /path/to/a /path/to/b

# open file
:e TAB or <nomefile>
