.bashrc in ambiente grafico viene eseguito con ogni nuovo terminale.




.bash_profile invece viene eseguito quando si accede a una login shell -via SSH o da console (CtrlAltF1/F2...). In quel caso .bashrc non viene eseguito e quindi lo si potrebbe voler richiamare da .bash_profile
source ~/.bashrc




Aggregate history of all terminals in the same .history :

shopt -s histappend
export HISTSIZE=100000
export HISTFILESIZE=100000
export HISTCONTROL=ignoredups:erasedups
export PROMPT_COMMAND="history -a;history -c;history -r;$PROMPT_COMMAND"




For color-enabled man pages, add the following to .bashrc :

man() {
    env \
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    man "$@"
}
